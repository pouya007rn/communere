from rest_framework import mixins, permissions, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from project_management.models import Project, Task
from project_management.serializers import ProjectSerializer, TaskSerializer, AssignTaskSerializer
from user_management.permissions import IsProjectManager


class ProjectViewSet(mixins.CreateModelMixin,
                     mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     GenericViewSet):
    serializer_class = ProjectSerializer
    permission_classes = [permissions.IsAuthenticated, IsProjectManager]
    queryset = Project.objects.all()


class TaskViewSet(mixins.CreateModelMixin,
                  mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  GenericViewSet):
    serializer_class = TaskSerializer
    permission_classes = [permissions.IsAuthenticated]
    queryset = Task.objects.all()

    @action(methods=['GET'], detail=True)
    def task_from_project(self, request, pk=None):
        queryset = self.queryset.filter(project__id=pk)
        serializer = self.serializer_class(queryset, many=True, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['GET'], detail=False)
    def developer_tasks(self, request):
        user = request.user
        queryset = self.queryset.filter(assignees=user)
        serializer = self.serializer_class(queryset, many=True, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)


class AssignTaskAPIView(APIView):
    serializer_class = AssignTaskSerializer
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, pk=None):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid()
        dev_list = serializer.validated_data.get('developers')
        task = Task.objects.get(pk=pk)
        if dev_list:
            assigned_developers = dev_list
        else:
            assigned_developers = [request.user.id]
        task.assign_developers(assigned_developers)

        return Response({'message': 'The task is successfully assigned.'})

    def get_permissions(self):
        dev_list = self.request.data.get('developers')
        self.permission_classes = [permissions.IsAuthenticated]
        if dev_list:
            self.permission_classes = [permissions.IsAuthenticated, IsProjectManager]
        return super(self.__class__, self).get_permissions()
