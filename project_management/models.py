from django.db import models

from user_management.models import CustomUser


class TimeStampedModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        ordering = ['-created_at']


class Project(TimeStampedModel):
    title = models.CharField(max_length=64)
    description = models.TextField(null=True, blank=True)
    reporter = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True,
                                 blank=True, related_name='reported_projects')

    def __str__(self):
        return self.title


class Task(TimeStampedModel):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='tasks')
    title = models.CharField(max_length=64)
    description = models.TextField(null=True, blank=True)
    reporter = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True,
                                 blank=True, related_name='reported_tasks')
    assignees = models.ManyToManyField(CustomUser, blank=True)

    def __str__(self):
        return self.title

    def assign_developers(self, developers: list):
        self.assignees.add(*developers)
        self.save()
