from rest_framework import serializers

from project_management.models import Project, Task
from user_management.serializers import SimpleCustomUserSerializer


class ProjectSerializer(serializers.ModelSerializer):
    reporter = SimpleCustomUserSerializer(read_only=True, many=False)

    class Meta:
        model = Project
        fields = '__all__'

    def create(self, validated_data):
        reporter = self.context.get('request').user
        project = Project.objects.create(reporter=reporter, **validated_data)
        return project


class TaskSerializer(serializers.ModelSerializer):
    reporter = SimpleCustomUserSerializer(read_only=True, many=False)
    assignees = SimpleCustomUserSerializer(read_only=True, many=True)

    class Meta:
        model = Task
        fields = '__all__'

    def create(self, validated_data):
        reporter = self.context.get('request').user
        task = Task.objects.create(reporter=reporter, **validated_data)
        return task

    def to_representation(self, instance):
        data = super(TaskSerializer, self).to_representation(instance)
        data['project'] = ProjectSerializer(instance.project, many=False).data
        return data


class AssignTaskSerializer(serializers.Serializer):
    developers = serializers.ListSerializer(required=False, child=serializers.IntegerField())


