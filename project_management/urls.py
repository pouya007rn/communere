from django.urls import path
from rest_framework.routers import DefaultRouter

from project_management.views import ProjectViewSet, TaskViewSet, AssignTaskAPIView

router = DefaultRouter()
router.register('project', ProjectViewSet, basename="project")
router.register('task', TaskViewSet, basename="task")


urlpatterns = [
    path('assign-task/<int:pk>/', AssignTaskAPIView.as_view())
]
urlpatterns += router.urls
