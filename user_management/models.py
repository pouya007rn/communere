from django.contrib.auth.models import AbstractUser
from django.db import models

from user_management.manager import UserManager


class RoleChoices(models.IntegerChoices):
    DEVELOPER = 1, 'developer'
    PROJECT_MANAGER = 2, 'project_manager'


class CustomUser(AbstractUser):
    role = models.PositiveSmallIntegerField(choices=RoleChoices.choices)
    manager = UserManager()
