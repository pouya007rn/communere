from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenObtainPairView

from .views import CreateUserViewSet

router = DefaultRouter()
router.register('create-user', CreateUserViewSet, basename="create-user")

urlpatterns = [
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
]

urlpatterns += router.urls
