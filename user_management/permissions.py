from rest_framework.permissions import BasePermission

from user_management.models import RoleChoices


class IsProjectManager(BasePermission):
    def has_permission(self, request, view):
        return request.user.role == RoleChoices.PROJECT_MANAGER

