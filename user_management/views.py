from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet
from .serializers import CreateUserSerializer


class CreateUserViewSet(mixins.CreateModelMixin, GenericViewSet):
    serializer_class = CreateUserSerializer
