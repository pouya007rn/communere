from rest_framework import serializers
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken

from .models import CustomUser


class SimpleCustomUserSerializer(serializers.ModelSerializer):
    role = serializers.CharField(source='get_role_display')

    class Meta:
        model = CustomUser
        fields = ['id', 'username', 'role']


class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['id', 'username', 'password', 'first_name', 'last_name', 'email', 'role']
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = CustomUser.objects.create_user(**validated_data)
        user.set_password(password)
        user.save()
        return user

    def to_representation(self, instance):
        data = super(CreateUserSerializer, self).to_representation(instance)
        data['access'] = str(AccessToken.for_user(instance))
        data['refresh'] = str(RefreshToken.for_user(instance))
        return data
